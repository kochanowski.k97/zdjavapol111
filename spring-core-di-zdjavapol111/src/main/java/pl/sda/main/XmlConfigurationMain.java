package pl.sda.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;

public class XmlConfigurationMain {

    public static void main(String[] args) {

        //utworzenie kontenera IoC
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("application-context.xml");

        PersonService personService = applicationContext.getBean("personService", PersonServiceImpl.class);

        personService.getAll().forEach(System.out::println);

        System.out.println(personService.getById("1"));

    }

}
