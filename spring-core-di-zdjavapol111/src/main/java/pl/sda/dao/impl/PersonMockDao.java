package pl.sda.dao.impl;

import pl.sda.dao.PersonDao;
import pl.sda.model.Person;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonMockDao implements PersonDao {

    private List<Person> persons;

    public PersonMockDao() {
        persons = Arrays.asList(
                new Person(1, "Michał", "Nowak"),
                new Person(2, "Anna", "Nowak"),
                new Person(3, "Jan", "Kowalski")
        );
    }

    @Override
    public Person getById(int id) {
        for (Person p: persons) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    @Override
    public List<Person> getAll() {
        return persons;
    }
}
