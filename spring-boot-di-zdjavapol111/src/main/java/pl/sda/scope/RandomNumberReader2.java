package pl.sda.scope;

import org.springframework.stereotype.Component;

@Component
public class RandomNumberReader2 {

    private final RandomNumberProvider randomNumberProvider;

    public RandomNumberReader2(RandomNumberProvider randomNumberProvider) {
        this.randomNumberProvider = randomNumberProvider;
    }

    public void printRandomNumber() {
        System.out.println(randomNumberProvider.getValue());
    }
}
