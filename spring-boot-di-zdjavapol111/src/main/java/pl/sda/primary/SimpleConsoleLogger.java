package pl.sda.primary;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class SimpleConsoleLogger implements SimpleLogger {

    @Override
    public void printMessage(String message) {
        System.out.println("CONSOLE: " + message);
    }
}
