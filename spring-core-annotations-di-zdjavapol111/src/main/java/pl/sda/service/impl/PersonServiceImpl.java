package pl.sda.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import pl.sda.dao.PersonDao;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.validator.PersonValidator;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    @Qualifier("personFileDao")
    @Autowired
    private PersonDao dao;

    private PersonValidator personValidator;

    @Override
    public Person getById(String id) {

        personValidator.validateId(id);

        final int parsedId = Integer.parseInt(id);

        return dao.getById(parsedId);
    }

    @Override
    public List<Person> getAll() {
        return dao.getAll();
    }

    public void setPersonValidator(PersonValidator personValidator) {
        this.personValidator = personValidator;
    }
}
